#pragma once

#include <inttypes.h>

#define PAYLOAD_CAPACITY 255

typedef struct Packet {
    uint8_t size;
    uint8_t type;
    uint8_t order;
    uint8_t payload[PAYLOAD_CAPACITY];
    uint32_t crc;
} Packet;
