#include "packet.h"

#include <math.h>
#include <stddef.h>
#include <string.h>

#define FRAME_CAPACITY 10

typedef struct Frame {
    Packet packets[FRAME_CAPACITY];
} Frame;

void frame_create(int packet_type, const uint8_t* const buf, const size_t size) {
    Frame f = {0};
    size_t rem_bytes = size;
    size_t offset = 0;
    uint8_t order = 1;
    uint8_t n_bytes = 0;
    uint8_t n_pkts = ceil(size / (double)PAYLOAD_CAPACITY);
    while (rem_bytes > 0) {
        if (rem_bytes < PAYLOAD_CAPACITY) {
            n_bytes = rem_bytes;
        } else {
            n_bytes = PAYLOAD_CAPACITY;
        }
        f.packets[n_pkts] = packet_create(packet_type, order, &buf[offset], n_bytes);

        offset += n_bytes;
        rem_bytes -= n_bytes;
        order += 1;
    }
}

Packet packet_create(int packet_type, uint8_t order, const uint8_t* const buf, const size_t size) {
    Packet p = {0};
    p.size = size;
    p.type = packet_type;
    p.order = order;
    memcpy(p.payload, buf, size);
    p.crc = 0xdead;

    return p;
}
